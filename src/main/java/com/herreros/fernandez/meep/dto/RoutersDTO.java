package com.herreros.fernandez.meep.dto;

public class RoutersDTO {

	private String id;

	private String name;

	private Double x;

	private Double y;

	private String licencePlate;

	private int range;

	private int seats;

	private String model;

	private String resourceImageId;

	private Double pricePerMinuteParking;

	private Double pricePerMinuteDriving;

	private boolean realTimeData;

	private String engineType;

	private String resourceType;

	private Long companyZoneId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}

	public String getLicencePlate() {
		return licencePlate;
	}

	public void setLicencePlate(String licencePlate) {
		this.licencePlate = licencePlate;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getResourceImageId() {
		return resourceImageId;
	}

	public void setResourceImageId(String resourceImageId) {
		this.resourceImageId = resourceImageId;
	}

	public Double getPricePerMinuteParking() {
		return pricePerMinuteParking;
	}

	public void setPricePerMinuteParking(Double pricePerMinuteParking) {
		this.pricePerMinuteParking = pricePerMinuteParking;
	}

	public Double getPricePerMinuteDriving() {
		return pricePerMinuteDriving;
	}

	public void setPricePerMinuteDriving(Double pricePerMinuteDriving) {
		this.pricePerMinuteDriving = pricePerMinuteDriving;
	}

	public boolean isRealTimeData() {
		return realTimeData;
	}

	public void setRealTimeData(boolean realTimeData) {
		this.realTimeData = realTimeData;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public Long getCompanyZoneId() {
		return companyZoneId;
	}

	public void setCompanyZoneId(Long companyZoneId) {
		this.companyZoneId = companyZoneId;
	}

	@Override
	public String toString() {
		return "Routers [id=" + id + ", name=" + name + ", x=" + x + ", y=" + y + ", licencePlate=" + licencePlate
				+ ", range=" + range + ", seats=" + seats + ", model=" + model + ", resourceImageId=" + resourceImageId
				+ ", pricePerMinuteParking=" + pricePerMinuteParking + ", pricePerMinuteDriving="
				+ pricePerMinuteDriving + ", realTimeData=" + realTimeData + ", engineType=" + engineType
				+ ", resourceType=" + resourceType + ", companyZoneId=" + companyZoneId + "]";
	}

}
