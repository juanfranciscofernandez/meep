package com.herreros.fernandez.meep.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.herreros.fernandez.meep.dto.RoutersDTO;
import com.herreros.fernandez.meep.model.Routers;

@Component
public class MapperRouters {
		
	public List<RoutersDTO> mapListToDTOList (List<Routers> listRouters){
		
		List<RoutersDTO> listRoutersDTO = new ArrayList<RoutersDTO>();
		
		for (Routers routers : listRouters) {
			RoutersDTO routersDTO = convert2DTO(routers);
			listRoutersDTO.add(routersDTO);		
		}
		
		return listRoutersDTO;
	}

	private RoutersDTO convert2DTO(Routers routers) {
		RoutersDTO routersDTO = new RoutersDTO();
		routersDTO.setId(routers.getId());
		routersDTO.setName(routers.getName());
		routersDTO.setX(routers.getX());
		routersDTO.setY(routers.getY());
		routersDTO.setLicencePlate(routers.getLicencePlate());
		routersDTO.setRange(routers.getRange());
		routersDTO.setSeats(routers.getSeats());
		routersDTO.setModel(routers.getModel());
		routersDTO.setResourceImageId(routers.getResourceImageId());
		routersDTO.setPricePerMinuteParking(routers.getPricePerMinuteParking());
		routersDTO.setPricePerMinuteDriving(routers.getPricePerMinuteDriving());
		routersDTO.setRealTimeData(routers.isRealTimeData());
		routersDTO.setEngineType(routers.getEngineType());
		routersDTO.setResourceType(routers.getResourceType());
		routersDTO.setCompanyZoneId(routers.getCompanyZoneId());
		return routersDTO;
	}

}
