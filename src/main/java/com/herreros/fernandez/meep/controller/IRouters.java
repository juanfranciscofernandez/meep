package com.herreros.fernandez.meep.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.herreros.fernandez.meep.dto.RoutersDTO;

public interface IRouters {

	ResponseEntity<List<RoutersDTO>> getRouters(String model);

}
