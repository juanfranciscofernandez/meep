package com.herreros.fernandez.meep.controller;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.herreros.fernandez.meep.dto.RoutersDTO;
import com.herreros.fernandez.meep.services.RoutersService;

@RestController
@RequestMapping("/api/public")
public class RoutersController implements IRouters {

	@Autowired
	private RoutersService routersService;
	
	@GetMapping(value = "/v1/routers", produces = MediaType.APPLICATION_JSON)
	@Override
	public ResponseEntity<List<RoutersDTO>> getRouters(@RequestParam(value = "model", required = false) String model) {
		return ResponseEntity.status(HttpStatus.OK).body(routersService.getRouters(model));
	}
	
}
