package com.herreros.fernandez.meep.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.herreros.fernandez.meep.dto.RoutersDTO;
import com.herreros.fernandez.meep.feign.RoutersFeign;
import com.herreros.fernandez.meep.mapper.MapperRouters;

@Service
public class RoutersService {

	@Autowired
	private  RoutersFeign routersFeign;
	
	@Autowired
	private  MapperRouters mapperRouters;
	
	public RoutersService(RoutersFeign routersFeign, MapperRouters mapperRouters) {
		super();
		this.routersFeign = routersFeign;
		this.mapperRouters = mapperRouters;
	}

	public List<RoutersDTO> getRouters(String model) {
		
		List<RoutersDTO> routersDTOList = new ArrayList<RoutersDTO>();
		
		if (model != null) {
			routersDTOList = mapToDTOList()
					.stream()
					.filter(x -> x.getModel().equals(model)).collect(Collectors.toList());
		} else {
			routersDTOList = mapToDTOList();
		}
		return routersDTOList;
	}
	
	private List<RoutersDTO> mapToDTOList() {
		return mapperRouters.mapListToDTOList(routersFeign.getRouters());
	}

}
