package com.herreros.fernandez.meep.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.herreros.fernandez.meep.model.Routers;

@FeignClient(name = "data", url = "${feign.client.url}")
public interface RoutersFeign {
 
    @RequestMapping(method = RequestMethod.GET)
    List<Routers> getRouters();
 
}
